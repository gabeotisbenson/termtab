#! /usr/bin/env node
const pkg			= require('./package');
const program	= require('commander');
const ug			= require('./modules/ug');

// Set up commander
program
	.version(pkg.version)
	.usage('[options] <search terms>')
	.option('-a, --artist <string>', 'Limit search results by artist')
	.option('-t, --type <string>', 'Limit search results by type (chords, tabs, bass, ukelele)', /^(tabs|chords|bass|ukulele)$/i, t => {
		return {
			tabs: 200,
			chords: 300,
			bass: 400,
			ukulele: 800
		}[t];
	})
	.parse(process.argv);

// Show help if no search terms or artist
if (program.args.length < 1) {
	program.outputHelp();
	process.exit(1);
}

// Replace pluses with spaces for the artist name
if (program.artist && program.artist.indexOf('+') !== -1) program.artist = program.artist.replace(/\+/g, ' ');

let args = {
	termsArr: program.args,
	artist: program.artist || null,
	type: program.type || null
};

(async () => {
	try {
		const songs = await ug.search(args);
		const bestSong = songs.reduce((final, current) => {
			if (!current.rating) current.rating = 0;
			if (!current.votes) current.votes = 0;
			if (!current.marketing_type && ug.bogusTypes.indexOf(current.type) === -1) {
				if (current.rating > final.rating) {
					final = current;
				} else if (current.rating === final.rating && current.votes > final.votes) {
					final = current;
				}
			}
			return final;
		}, { rating: -1, votes: -1 });
		try {
			const tab = await ug.getTab(bestSong.tab_url);
			console.log([`'${bestSong.song_name}' by ${bestSong.artist_name}`, tab].join('\n\n'));
		} catch (err) {
			console.log('Sorry, the tab could not be found.');
		}
	} catch (err) {
		console.log('Sorry, no results were found.');
	}
})();
