// Required modules
const fetch		= require('node-fetch');
const Promise	= require('bluebird');

// Regular expression for grabbing the JSON content
const contentRgx = /window\.UGAPP\.store\.page = ({.+});/;

// Default arguments
const defaultArgs = {
	termsArr: [],
	page: 1
};

// Takes in some arguments, and returns a UG url to match
const getURL = args => {
	args = Object.assign(defaultArgs, args);
	const ugPre = 'https://www.ult' + 'imate-gui' + 'tar.com/search.php';
	if (args.artist) args.termsArr.unshift(args.artist);
	const terms = encodeURI(args.termsArr.join(' '));
	let url = ugPre + '?title=' + terms;
	if (args.type) url += '&type=' + args.type;
	if (args.page) url += '&page=' + args.page;
	return url;
};

const search = args => {
	return new Promise(async (resolve, reject) => {
		const url = getURL(args);
		try {
			const response = await fetch(url);
			const body = await response.text();
			if (!contentRgx.test(body)) {
				reject(new Error('Search results could not be parsed'));
				return;
			}
			const dataBlob = JSON.parse(body.match(contentRgx)[1]).data;
			const songs = dataBlob.results;
			if (!songs || songs.length < 1) {
				reject(new Error('No songs were found'));
				return;
			}
			resolve(songs);
		} catch (err) {
			reject(err);
		}
	});
};

const getTab = url => {
	return new Promise(async (resolve, reject) => {
		try {
			const response = await fetch(url);
			const body = await response.text();
			resolve(JSON.parse(body.match(contentRgx)[1]).data.tab_view.wiki_tab.content.replace(/\[\/?ch\]/g, ''));
		} catch (err) {
			reject(err);
		}
	});
};

const bogusTypes = ['Pro', 'Power'];

module.exports = {
	search,
	getTab,
	bogusTypes
};
