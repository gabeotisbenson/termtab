# termtab
A command line interface for finding and displaying guitar chords and tabs

```shell
$ termtab

	Usage: termtab [options] <search terms>


	Options:

		-V, --version          output the version number
		-a, --artist <string>  Limit search results by artist
		-t, --type <string>    Limit search results by type (chords, tabs, bass, ukelele)
		-h, --help             output usage information

```
